# CAKE PHP & ANGULAR #

I separate the folders between the backend and the front end


## BACKEND ##
### URL ###
* Home page = /home_page
* API Blog = /api/posts.json
* API Blog Detail = /api/posts/1.json

### CONFIG ###
* database = config/database
* .sql = dirDB



## FRONT END ##

### RUNNING ###
* cd folder project
* npm install http-server -g
* http-server
* OR running use like xampp, wamp and other

### CONFIG ###
* Url Webservice: apps/app.js -> webServiceUrl

### ACCESS PAGE ###
* home_page : http://127.0.0.1:8081/#/
* blog: http://127.0.0.1:8081/#/blog
* blog detail:http://127.0.0.1:8081/#/blog_detail/1
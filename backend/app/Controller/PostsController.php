<?php
    
    /**
    * FILE PostController.php
    * created by hidayat
    * Coding with EAT MORE ^.^
    */

    class PostsController extends AppController {

        public $components = array('RequestHandler');

        // blog post
        public function index() {
            $post = $this->Post->find('all');
            $this->set(array(
                'data' => $post,
                '_serialize' => array('data')
            ));
        }

        // blog post detail
        public function view($id) {
            $post = $this->Post->findByid_post($id);
            $this->set(array(
                'data' => $post,
                '_serialize' => array('data')
            ));
        }
    }

?>
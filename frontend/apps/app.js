var app = angular.module('BlogApp', ['ngRoute']);

var webServiceUrl = 'http://localhost:8080/project_cake/source/PHP_Project/backend/';

// config
app.config(function ($routeProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'templates/home/index.html',
            controller: 'home_page'
        })

        .when('/blog', {
            templateUrl: 'templates/blog/index.html',
            controller: 'blog'
        })

        .when('/blog_detail/:id', {
            templateUrl: 'templates/blog/detail.html',
            controller: 'blog_detail'
        })

        .otherwise({
            redirectTo: '/'
        });

});


// home page contorller
app.controller('home_page', function($scope, $http){
    
    $scope.wsUrl = webServiceUrl;
    
    // youtube slide
    angular.element(".bxslider").bxSlider();

    // nav top
    angular.element(window).scroll(function() {
        if($(document).scrollTop() > 150) {
            angular.element(".nav-top-custom").css('background-color','rgba(0,0,0,0.45)');
        }
        else {
            angular.element(".nav-top-custom").css('background-color','transparent');
        }
    });

    // get blogs
    $http.get(webServiceUrl + 'api/posts.json')
    .success(function(response){
        $scope.blogs = response.data;
    });
});

// blog controller
app.controller('blog', function($scope, $http){
    
    $scope.wsUrl = webServiceUrl;

    // get blogs
    $http.get(webServiceUrl + 'api/posts.json')
    .success(function(response){
        $scope.blogs = response.data;
    });

});

// blog detail controller
app.controller('blog_detail', function($scope, $http, $routeParams){
    
    $scope.blog_id = $routeParams.id;
    $scope.wsUrl = webServiceUrl;

    // get blogs
    $http.get(webServiceUrl + 'api/posts.json')
    .success(function(response){
        $scope.blogs = response.data;
    });

    // get blogs detail
    $http.get(webServiceUrl + 'api/posts/'+$scope.blog_id+'.json')
    .success(function(response){
        $scope.blogs_detail = response.data;
    });

});